//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 28/10/23.
//

import Fluent
import Vapor
import ShiftsAppSharedDTO

struct MetadataController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        let metadataRoutes = routes.grouped("api", "v2", "data")
        metadataRoutes.get(use: getData)
    }
    
    func getData(_ req: Request) async throws -> MetadataResponseDTO {
        let dataArray = try await Metadata.query(on: req.db)
            .with(\.$shiftsGroups) { shiftsGroup in
                shiftsGroup.with(\.$shifts)
            }
            .all()
        
        guard dataArray.count == 1 else {
            throw Abort(.preconditionFailed)
        }
        
        return dataArray.compactMap(MetadataResponseDTO.init).first!
    }
}
