import Fluent
import ShiftsAppSharedDTO
import Vapor

struct ShiftsGroupController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        let publicShiftsGroupRoutes = routes.grouped("api", "v2", "groups")
        publicShiftsGroupRoutes.get(use: getAllHandler)
        publicShiftsGroupRoutes.get(":groupID", use: getHandler)
        publicShiftsGroupRoutes.get(":groupID", "shifts", use: getShiftsHandler)
        publicShiftsGroupRoutes.get("sorted", use: sortedHandler)
        
//        let privateShiftsGroupRoutes = publicShiftsGroupRoutes.grouped(JWTAuthenticator())
//        privateShiftsGroupRoutes.post(use: createHandler)
//        privateShiftsGroupRoutes.put(":groupID", use: updateHandler)
//        privateShiftsGroupRoutes.delete(":groupID", use: deleteHandler)
        
        
        publicShiftsGroupRoutes.post(use: createHandler)
        publicShiftsGroupRoutes.put(":groupID", use: updateHandler)
        publicShiftsGroupRoutes.delete(":groupID", use: deleteHandler)
    }
    
    // MARK: - Public routes
    // GET /api/groups
    func getAllHandler(_ req: Request) async throws -> [ShiftsGroupResponseDTO] {
        let groups = try await ShiftsGroup.query(on: req.db)
            .with(\.$shifts)
            .all()
            .compactMap(ShiftsGroupResponseDTO.init)
        
        return groups
    }
    
    // GET /api/groups/{groupID}
    func getHandler(_ req: Request) async throws -> ShiftsGroupResponseDTO {
        guard let group = try await ShiftsGroup.find(req.parameters.get("groupID"), on: req.db) else {
            throw Abort(.notFound)
        }
        
        try await group.$shifts.load(on: req.db)
        
        guard let shiftsGroupResponseDTO = ShiftsGroupResponseDTO(group) else {
            throw Abort(.internalServerError)
        }
        return shiftsGroupResponseDTO
    }
    
    // GET /api/groups/{groupID}/shifts
    func getShiftsHandler(_ req: Request) async throws -> [ShiftResponseDTO] {
        guard let group = try await ShiftsGroup.find(req.parameters.get("groupID"), on: req.db) else {
            throw Abort(.notFound)
        }
        return try await group
            .$shifts
            .query(on: req.db)
            .all()
            .compactMap(ShiftResponseDTO.init)
    }
    
    // GET /api/groups/sorted
    func sortedHandler(_ req: Request) async throws -> [ShiftsGroupResponseDTO] {
        try await ShiftsGroup.query(on: req.db)
            .sort(\.$validFrom, .descending)
            .with(\.$shifts)
            .all()
            .compactMap(ShiftsGroupResponseDTO.init)
    }
    
    // MARK: - Protected routes
    // POST /api/groups
    func createHandler(_ req: Request) async throws -> ShiftsGroupResponseDTO {
        guard let metadata = try await Metadata
            .query(on: req.db)
            .first()
        else {
            throw Abort(.internalServerError)
        }

        let shiftsGroupRequestDTO = try req.content.decode(ShiftsGroupRequestDTO.self)
        
        let shiftsGroup = ShiftsGroup(
            validFrom: shiftsGroupRequestDTO.validFrom,
            category: shiftsGroupRequestDTO.category.rawValue,
            location: shiftsGroupRequestDTO.location.rawValue,
            metadataID: try metadata.requireID()
        )
        try await shiftsGroup.save(on: req.db)
        try await shiftsGroup.$shifts.load(on: req.db)
        
        guard let shiftsGroupResponseDTO = ShiftsGroupResponseDTO(shiftsGroup) else {
            throw Abort(.internalServerError)
        }
        
        metadata.updatedAt = .now
        try await metadata.save(on: req.db)
        
        return shiftsGroupResponseDTO
    }
    
    // PUT /api/groups/{groupID}
    func updateHandler(_ req: Request) async throws -> ShiftsGroupResponseDTO {
        let updatedShiftsGroup = try req.content.decode(ShiftsGroupRequestDTO.self)
        
        guard let shiftsGroup = try await ShiftsGroup.find(req.parameters.get("groupID"), on: req.db) else {
            throw Abort(.notFound, reason: "ShiftsGroup not found")
        }
        
        guard let metadata = try await Metadata.query(on: req.db).first() else {
            throw Abort(.internalServerError)
        }
        
        shiftsGroup.validFrom = updatedShiftsGroup.validFrom
        shiftsGroup.category = updatedShiftsGroup.category.rawValue
        shiftsGroup.location = updatedShiftsGroup.location.rawValue
        
        try await shiftsGroup.save(on: req.db)
        try await shiftsGroup.$shifts.load(on: req.db)
        
        guard let shiftsGroupResponseDTO = ShiftsGroupResponseDTO(shiftsGroup) else {
            throw Abort(.internalServerError)
        }
        
        metadata.updatedAt = .now
        try await metadata.save(on: req.db)
        
        return shiftsGroupResponseDTO
    }
    
    // DELETE /api/groups/{groupID}
    func deleteHandler(_ req: Request) async throws -> HTTPStatus {
        guard let shiftsGroup = try await ShiftsGroup.find(req.parameters.get("groupID"), on: req.db) else {
            throw Abort(.notFound)
        }
        
        guard let metadata = try await Metadata.query(on: req.db).first() else {
            throw Abort(.internalServerError)
        }
        
        metadata.updatedAt = .now
        try await metadata.save(on: req.db)
        
        try await shiftsGroup.delete(on: req.db)
        
        return .ok
    }
}
