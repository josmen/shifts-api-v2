import Fluent
import Vapor
import ShiftsAppSharedDTO

class UserController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        let publicUserRoutes = routes.grouped("api", "v2", "users")
        publicUserRoutes.post("login", use: loginHandler)
        
        let privateUserRoutes = publicUserRoutes.grouped(JWTAuthenticator())
        privateUserRoutes.post("register", use: createHandler)
        privateUserRoutes.get(use: getAllUsersHandler)
        privateUserRoutes.delete(":userId", use: deleteHandler)
    }
    
    // MARK: - Public routes
    func loginHandler(req: Request) async throws -> LoginResponseDTO {
        let user = try req.content.decode(LoginRequestDTO.self)
        
        guard let existingUser = try await User.query(on: req.db).filter(\.$username == user.username).first() else {
            return LoginResponseDTO(error: true, reason: "Username is not found.")
        }
        
        let result = try await req.password.async.verify(user.password, created: existingUser.password)
        if !result {
            return LoginResponseDTO(error: true, reason: "Password is incorrect.")
        }
        
        let authPayload = try AuthPayload(expiration: .init(value: .distantFuture), userId: existingUser.requireID())
        return try LoginResponseDTO(error: false, token: req.jwt.sign(authPayload), userId: existingUser.requireID())
    }
    
    // MARK: - Protected routes
    func createHandler(req: Request) async throws -> RegisterResponseDTO {
        try User.validate(content: req)
        
        let user = try req.content.decode(User.self)
        
        if let existingUser = try await User.query(on: req.db).filter(\.$username == user.username).first() {
            throw Abort(.conflict, reason: "User with username \(existingUser.username) already exists")
        }
        
        user.password = try await req.password.async.hash(user.password)
        
        try await user.save(on: req.db)
        return RegisterResponseDTO(error: false)
    }
    
    func getAllUsersHandler(req: Request) async throws -> [UserResponseDTO] {
        return try await User
            .query(on: req.db)
            .all()
            .compactMap(UserResponseDTO.init)
    }
    
    func deleteHandler(req: Request) async throws -> HTTPStatus {
        guard let userId = req.parameters.get("userId", as: UUID.self) else {
            throw Abort(.badRequest)
        }
        
        guard let user = try await User
            .query(on: req.db)
            .filter(\.$id == userId)
            .first() else {
            throw Abort(.notFound)
        }
        
        try await user.delete(on: req.db)
        return .ok
    }
}
