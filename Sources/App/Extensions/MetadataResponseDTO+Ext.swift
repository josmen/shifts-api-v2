//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 29/10/23.
//

import ShiftsAppSharedDTO
import Vapor

extension MetadataResponseDTO: Content {
    init?(_ metadata: Metadata) {
        self.init(updatedAt: metadata.updatedAt, shiftsGroups: metadata.shiftsGroups.compactMap(ShiftsGroupResponseDTO.init))
    }
}
