import ShiftsAppSharedDTO
import Vapor

extension ShiftResponseDTO: Content {
    init?(_ shift: Shift) {
        guard let id = shift.id else { return nil }
        
        self.init(
            id: id,
            name: shift.name,
            startHour: shift.startHour,
            startMinute: shift.startMinute,
            endHour: shift.endHour,
            endMinute: shift.endMinute,
            saturation: shift.saturation,
            shiftsGroupID: shift.$shiftsGroup.id
        )
    }
}
