import ShiftsAppSharedDTO
import Vapor

extension ShiftsGroupResponseDTO: Content {
    init?(_ shiftsGroup: ShiftsGroup) {
        guard let id = shiftsGroup.id,
              let category = Category(rawValue: shiftsGroup.category),
              let location = Location(rawValue: shiftsGroup.location)
        else { return nil }
        let shifts = shiftsGroup.shifts.compactMap(ShiftResponseDTO.init)
        self.init(id: id, validFrom: shiftsGroup.validFrom, category: category, location: location, shifts: shifts)
    }
}
