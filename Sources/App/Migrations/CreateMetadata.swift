//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 28/10/23.
//

import Fluent

struct CreateMetadata: AsyncMigration {
    func prepare(on database: Database) async throws {
        try await database.schema(Metadata.schema)
            .id()
            .field(Metadata.FieldKeys.updatedAt, .datetime, .required)
            .create()
    }
    
    func revert(on database: Database) async throws {
        try await database.schema(Metadata.schema).delete()
    }
}

struct CreateDefaultMetadata: AsyncMigration {
    func prepare(on database: Database) async throws {
        let metadata = Metadata(updatedAt: Date.now)
        try await metadata.create(on: database)
    }
    
    func revert(on database: Database) async throws {
        try await Metadata.query(on: database).all().delete(on: database)
    }
}
