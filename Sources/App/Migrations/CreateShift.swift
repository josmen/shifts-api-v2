import Fluent

struct CreateShift: AsyncMigration {
    func prepare(on database: Database) async throws {
        try await database.schema(Shift.schema)
            .id()
            .field(Shift.FieldKeys.name, .string, .required)
            .field(Shift.FieldKeys.startHour, .int, .required)
            .field(Shift.FieldKeys.startMinute, .int, .required)
            .field(Shift.FieldKeys.endHour, .int, .required)
            .field(Shift.FieldKeys.endMinute, .int, .required)
            .field(Shift.FieldKeys.saturation, .double, .required)
            .field(Shift.FieldKeys.shiftsGroup, .uuid)
            .foreignKey(Shift.FieldKeys.shiftsGroup, references: ShiftsGroup.schema, .id, onDelete: .cascade)
            .create()
    }
    
    func revert(on database: Database) async throws {
        try await database.schema(Shift.schema).delete()
    }
}
