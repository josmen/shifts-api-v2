import Fluent

struct CreateShiftsGroup: AsyncMigration {
    func prepare(on database: Database) async throws {
        try await database.schema(ShiftsGroup.schema)
            .id()
            .field(ShiftsGroup.FieldKeys.validFrom, .date, .required)
            .field(ShiftsGroup.FieldKeys.category, .string, .required)
            .field(ShiftsGroup.FieldKeys.location, .string, .required)
            .field(ShiftsGroup.FieldKeys.metadata, .uuid)
            .foreignKey(ShiftsGroup.FieldKeys.metadata, references: Metadata.schema, .id, onDelete: .setNull)
            .create()
    }
    
    func revert(on database: Database) async throws {
        try await database.schema(ShiftsGroup.schema).delete()
    }
}
