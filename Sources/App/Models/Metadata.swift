//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 28/10/23.
//

import Fluent
import Vapor

final class Metadata: Model, Content {
    static var schema: String = "metadata"
    
    struct FieldKeys {
        static var updatedAt: FieldKey { "updated_at" }
    }
    
    @ID
    var id: UUID?
    
    @Field(key: FieldKeys.updatedAt)
    var updatedAt: Date
    
    @Children(for: \ShiftsGroup.$metadata)
    var shiftsGroups: [ShiftsGroup]
    
    init() { }
    
    init(id: UUID? = nil, updatedAt: Date) {
        self.id = id
        self.updatedAt = updatedAt
    }
}
