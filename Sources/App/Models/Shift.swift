import Fluent
import Vapor

final class Shift: Model, Content, Validatable {
    static var schema: String = "shifts"
    
    struct FieldKeys {
        static var name: FieldKey { "name" }
        static var startHour: FieldKey { "start_hour" }
        static var startMinute: FieldKey { "start_minute" }
        static var endHour: FieldKey { "end_hour" }
        static var endMinute: FieldKey { "end_minute" }
        static var saturation: FieldKey { "saturation" }
        static var shiftsGroup: FieldKey { "group_id" }
    }
    
    @ID
    var id: UUID?
    
    @Field(key: FieldKeys.name)
    var name: String
    
    @Field(key: FieldKeys.startHour)
    var startHour: Int
    
    @Field(key: FieldKeys.startMinute)
    var startMinute: Int
    
    @Field(key: FieldKeys.endHour)
    var endHour: Int
    
    @Field(key: FieldKeys.endMinute)
    var endMinute: Int
    
    @Field(key: FieldKeys.saturation)
    var saturation: Double
    
    @Parent(key: FieldKeys.shiftsGroup)
    var shiftsGroup: ShiftsGroup
    
    init() { }
    
    init(
        id: UUID? = nil,
        name: String,
        startHour: Int,
        startMinute: Int,
        endHour: Int,
        endMinute: Int,
        saturation: Double,
        shiftsGroupID: ShiftsGroup.IDValue
    ) {
        self.id = id
        self.name = name
        self.startHour = startHour
        self.startMinute = startMinute
        self.endHour = endHour
        self.endMinute = endMinute
        self.saturation = saturation
        self.$shiftsGroup.id = shiftsGroupID
    }
    
    static func validations(_ validations: inout Validations) {
        validations.add("startHour", as: Int.self, is: .range(0..<24), customFailureDescription: "Start hour is incorrect")
        validations.add("startMinute", as: Int.self, is: .range(0..<60), customFailureDescription: "Start minute is incorrect")
        validations.add("endHour", as: Int.self, is: .range(0..<24), customFailureDescription: "End hour is incorrect")
        validations.add("endMinute", as: Int.self, is: .range(0..<60), customFailureDescription: "End minute is incorrect")
    }
}
