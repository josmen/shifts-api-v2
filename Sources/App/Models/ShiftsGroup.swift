import Fluent
import Vapor

final class ShiftsGroup: Model, Content {
    static var schema: String = "shifts_groups"
    
    struct FieldKeys {
        static var validFrom: FieldKey { "valid_from" }
        static var category: FieldKey { "category" }
        static var location: FieldKey { "location" }
        static var metadata: FieldKey { "metadata_id" }
    }
    
    @ID
    var id: UUID?
    
    @Field(key: FieldKeys.validFrom)
    var validFrom: Date
    
    @Field(key: FieldKeys.category)
    var category: String
    
    @Field(key: FieldKeys.location)
    var location: String
    
    @Parent(key: FieldKeys.metadata)
    var metadata: Metadata
    
    @Children(for: \Shift.$shiftsGroup)
    var shifts: [Shift]
    
    init() { }
    
    init(id: UUID? = nil, validFrom: Date, category: String, location: String, metadataID: Metadata.IDValue) {
        self.id = id
        self.validFrom = validFrom
        self.category = category
        self.location = location
        self.$metadata.id = metadataID
    }
}
