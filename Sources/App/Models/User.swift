import Fluent
import Vapor

final class User: Model, Content, Validatable {
    static let schema = "users"
    
    struct FieldKeys {
        static var username: FieldKey { "username" }
        static var password: FieldKey { "password" }
        static var isAdmin: FieldKey { "is_admin" }
    }

    @ID(key: .id)
    var id: UUID?

    @Field(key: FieldKeys.username)
    var username: String

    @Field(key: FieldKeys.password)
    var password: String
    
    @Field(key: FieldKeys.isAdmin)
    var isAdmin: Bool

    init() { }

    init(id: UUID? = nil, username: String, password: String, isAdmin: Bool) {
        self.id = id
        self.username = username
        self.password = password
        self.isAdmin = isAdmin
    }

    static func validations(_ validations: inout Validations) {

        validations.add(
            "username",
            as: String.self,
            is: !.empty,
            customFailureDescription: "Username is required")
        validations.add(
            "password",
            as: String.self,
            is: !.empty,
            customFailureDescription: "Password is required")

        validations.add(
            "username",
            as: String.self,
            is: .count(3...) && .alphanumeric,
            customFailureDescription: "Username must be at least 3 characters long and alphanumeric")
        validations.add(
            "password",
            as: String.self,
            is: .count(8...),
            customFailureDescription: "Password must be at least 8 characters long")
    }
}
