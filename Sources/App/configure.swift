import NIOSSL
import Fluent
import FluentPostgresDriver
import Vapor

// configures your application
public func configure(_ app: Application) async throws {
    // uncomment to serve files from /Public folder
    // app.middleware.use(FileMiddleware(publicDirectory: app.directory.publicDirectory))
    
    // server port
    app.http.server.configuration.port = Int(Environment.get("PORT") ?? "8080" ) ?? 8080

    app.databases.use(DatabaseConfigurationFactory.postgres(configuration: .init(
        hostname: Environment.get("DATABASE_HOST") ?? "localhost",
        port: Environment.get("DATABASE_PORT").flatMap(Int.init(_:)) ?? SQLPostgresConfiguration.ianaPortNumber,
        username: Environment.get("DATABASE_USERNAME") ?? "vapor_username",
        password: Environment.get("DATABASE_PASSWORD") ?? "vapor_password",
        database: Environment.get("DATABASE_NAME") ?? "vapor_database",
        tls: .prefer(try .init(configuration: .clientDefault)))
    ), as: .psql)

    app.migrations.add(CreateMetadata())
    app.migrations.add(CreateDefaultMetadata())
    app.migrations.add(CreateShiftsGroup())
    app.migrations.add(CreateShift())
    app.migrations.add(CreateUser())
    app.migrations.add(CreateAdmin())
    
    try await app.autoMigrate()
    
    app.jwt.signers.use(.hs256(key: Environment.get("JWT_KEY") ?? "MySuperSecretKey"))

    // register routes
    try routes(app)
}
