import Fluent
import Vapor

func routes(_ app: Application) throws {
    try app.register(collection: UserController())
    try app.register(collection: ShiftsGroupController())
    try app.register(collection: ShiftController())
    try app.register(collection: MetadataController())
}
